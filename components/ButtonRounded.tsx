import React from "react";

const ButtonRounded = ({ active, children, className, onclick }: any) => {
    return (
        <button
            className={`rounded-full flex w-10 h-10 items-center justify-center bg-stone-100  ${
                active && "border-stone-400 border"
            } ${className}`}
            onClick={onclick}
        >
            {children}
        </button>
    );
};

export default ButtonRounded;
