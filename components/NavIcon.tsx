import React from "react";

const NavIcon = ({ active, icon, text }: any) => {
    return (
        <button
            className={`${
                active && "bg-primary"
            } flex flex-col items-center justify-center gap-2 w-15 h-18 aspect-square text-gray-600 rounded-2xl text-sm p-4 appearance-none`}
        >
            <div className="w-8 h-8 flex items-center justify-center">{icon}</div>
            <div className={`${active && 'text-white'}`}>{text}</div>
        </button>
    );
};

export default NavIcon;
