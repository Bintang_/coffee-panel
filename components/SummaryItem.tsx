/* eslint-disable @next/next/no-img-element */
import React from "react";
import { BsFillPencilFill } from "react-icons/bs";

const SummaryItem = ({ data }: any) => {
    return (
        <div className="flex gap-4 w-full">
            <img src={data.image} alt="" className="w-20 h-20 rounded-lg" />
            <div className="space-y-2 w-full">
                <div className="font-semibold">{data?.title}</div>
                <div className="flex">
                    <div className="w-12 font-semibold">x 1</div>
                    <div className="w-full px-5">
                        <button className="flex gap-4 text-stone-600 items-center px-4 rounded-lg test-xs bg-stone-200">
                            Edit
                            <BsFillPencilFill className="w-3 h-3" />
                        </button>
                    </div>
                    <div>
                        <span className="text-xs">$</span>
                        {data?.price}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SummaryItem;
