/* eslint-disable @next/next/no-img-element */
import React from "react";
import SummaryItem from "./SummaryItem";
import Button from "./Button";
import { AiOutlineScan } from "react-icons/ai";
import { BsFillCreditCardFill } from "react-icons/bs";
import { FaMoneyBillWave } from "react-icons/fa";

const Summary = ({ data, setModal }: any) => {
    let subTotal = data
        ?.map((el: any) => el.price)
        .reduce((a: any, b: any) => a + b, 0);
    let tax = subTotal / 10;
    let total = subTotal + tax;

    return (
        <div className="sticky top-0 right-0 w-1/3 min-h-[100vh] h-0 overflow-auto bg-white z-50 p-4 px-8 flex flex-col gap-8 justify-between">
            <div className="flex gap-4 items-center">
                <img
                    alt=""
                    src="https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    className="w-16 h-16 aspect-square rounded-2xl object-cover"
                ></img>
                <div className="w-full space-y-2">
                    <div className="text-xs text-gray-600">{`I'am a Cashier`}</div>
                    <div className="font-semi-bold">Jelly Grande</div>
                </div>
            </div>
            {data.length > 0 && (
                <>
                    <div className="h-full space-y-8">
                        <h3 className="text-2xl font-semibold">Bills</h3>
                        <div className="flex flex-col gap-8">
                            {data?.map((el: any, index: any) => {
                                return <SummaryItem data={el} key={index} />;
                            })}
                        </div>

                        <div className="py-8 space-y-2">
                            <div className="flex justify-between">
                                <div>Subtotal</div>
                                <div key={data}>
                                    <span className="text-xs">$</span>
                                    {subTotal}
                                </div>
                            </div>
                            <div className="flex justify-between text-gray-600">
                                <div>Tax</div>
                                <div>
                                    <span className="text-xs">$</span>
                                    {tax.toFixed(2)}
                                </div>
                            </div>
                            <div className="w-full border-b-2 border-dashed"></div>
                            <div className="flex justify-between font-bold">
                                <div>Total</div>
                                <div>
                                    <span className="text-xs">$</span>
                                    {total.toFixed(3)}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="space-y-10">
                        <div className="text-2xl font-semibold">
                            Payment Method
                        </div>
                        <div className="flex gap-4 text-xs">
                            <div className="flex flex-col items-center gap-3 w-full p-4 rounded-2xl bg-stone-400 opacity-50">
                                <div>
                                    <FaMoneyBillWave className="w-8 h-8" />
                                </div>
                                <div>Cash</div>
                            </div>
                            <div className="flex flex-col items-center gap-3 w-full p-4 rounded-2xl bg-stone-400 border border-stone-700">
                                <div>
                                    <BsFillCreditCardFill className="w-8 h-8" />
                                </div>
                                <div>Debit Card</div>
                            </div>
                            <div className="flex flex-col items-center gap-3 w-full p-4 rounded-2xl bg-stone-400 opacity-50">
                                <div>
                                    <AiOutlineScan className="w-8 h-8" />
                                </div>
                                <div>E-wallet</div>
                            </div>
                        </div>
                        <Button
                            text="Download Receipt"
                            onclick={() => setModal(true)}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default Summary;
