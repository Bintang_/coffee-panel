import React from "react";

const Button = ({ text, onclick }: any) => {
    return (
        <button onClick={onclick} className="w-full px-4 py-3 text-center bg-primary text-white rounded-2xl font-semibold">
            {text}
        </button>
    );
};

export default Button;
