import React from "react";
import NavIcon from "./NavIcon";
import { GrHomeRounded } from "react-icons/gr";
import { AiOutlineClockCircle } from "react-icons/ai";
import { BiWallet } from "react-icons/bi";
import { TbDiscount } from "react-icons/tb";
import { BsGearFill } from "react-icons/bs";
import { HiBookOpen, HiOutlineLogout } from "react-icons/hi";

const Sidebar = () => {
    return (
        <div className="sticky top-0 left-0 w-40 h-[100vh] bg-white z-50 flex flex-col gap-2">
            <div className="h-28 flex items-center justify-center">
                <span className="text-4xl italic font-extrabold text-primary">
                    K
                </span>
            </div>
            <div className="h-full flex flex-col items-center gap-2">
                <NavIcon
                    text="Home"
                    icon={
                        <GrHomeRounded className="w-[80%] h-[80%] text-white" />
                    }
                />
                <NavIcon
                    active
                    text="Menu"
                    icon={<HiBookOpen className="w-full h-full text-white" />}
                />
                <NavIcon
                    text="History"
                    icon={<AiOutlineClockCircle className="w-full h-full" />}
                />
                <NavIcon
                    text="Wallet"
                    icon={<BiWallet className="w-full h-full" />}
                />
                <NavIcon
                    text="Promos"
                    icon={<TbDiscount className="w-full h-full" />}
                />
                <NavIcon
                    text="Setting"
                    icon={<BsGearFill className="w-full h-full" />}
                />
            </div>
            <div className="h-32 flex flex-col items-center gap-4">
                <NavIcon
                    text="Logout"
                    icon={<HiOutlineLogout className="w-full h-full" />}
                />
            </div>
        </div>
    );
};

export default Sidebar;
