import React from "react";
import ReactModal from "react-modal";
import { useOnClickOutside } from "@/hooks/useHook";

type ModalProps = {
    width?: string;
    show: boolean;
    onClose?: (open: boolean) => void;
    disabled?: boolean;
    children: React.ReactNode;
};

export const Modal: React.FC<ModalProps> = (props) => {
    const ref = React.useRef<HTMLDivElement>(null);

    const [modalOpen, setModalOpen] = React.useState(props.show);

    useOnClickOutside(ref, () => {
        if (modalOpen && !props.disabled) {
            setModalOpen(false);
            if (props.onClose !== undefined) props.onClose(false);
        }
    });

    React.useEffect(() => {
        setModalOpen(props.show);
    }, [props.show]);

    return (
        <ReactModal
            isOpen={modalOpen}
            onRequestClose={() => {
                setModalOpen(false);
                if (props.onClose !== undefined) props.onClose(false);
            }}
            className="absolute top-0 left-0 z-100 w-full h-screen bg-black bg-opacity-75 flex items-center justify-center"
            ariaHideApp={false}
        >
            <div
                ref={ref}
                className={`${props.width} rounded-[4px] py-4 px-2 lg:px-10`}
            >
                {props.children}
            </div>
        </ReactModal>
    );
};

Modal.defaultProps = {
    width: "w-max",
    disabled: false,
};
