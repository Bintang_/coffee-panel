import React from "react";
import { BiSearch } from "react-icons/bi";

const Search = ({ onChange }: any) => {
    return (
        <label className="relative w-full">
            <input
                type="text"
                onChange={(e:any) => onChange(e.target.value)}
                className="w-full appearance-none px-4 py-2 bg-white rounded-lg"
                placeholder="Search category or menu..."
            />
            <BiSearch className="absolute right-5 top-2 w-6 h-6" />
        </label>
    );
};

export default Search;
