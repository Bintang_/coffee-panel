import React from "react";

type TCatIcon = {
    active?: boolean;
    icon: React.ReactNode;
    text: string;
    onClick: any;
};

const CategoryIcon = ({ active = false, icon, text, onClick }: TCatIcon) => {
    return (
        <div
            onClick={onClick}
            className={`rounded-3xl select-none cursor-pointer border ${
                active
                    ? "bg-stone-400 border-stone-800 "
                    : "bg-white border-neutral-300"
            } p-4 bg-opacity-75  w-full aspect-[4/5] flex flex-col items-center justify-center gap-2`}
        >
            <div className="w-8 h-8 m-auto text-center">
                {icon}
            </div>
            <div className="text-sm text-center capitalize">{text}</div>
        </div>
    );
};

export default CategoryIcon;
