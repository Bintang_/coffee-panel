/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import ButtonRounded from "./ButtonRounded";
import Button from "./Button";
import variants from "../data/variants.json";
import { BsFire } from "react-icons/bs";
import { IoMdSnow } from "react-icons/io";

const Card = ({ data, action }: any) => {
    const [mood, setMood] = useState<any>();
    const [size, setSize] = useState<any>();
    const [sugar, setSugar] = useState<any>();
    const [ice, setIce] = useState<any>();

    const handleClick = () => {
        action({ ...data, mood, size, sugar, ice });
    };

    return (
        <div className="rounded-lg bg-white p-8 space-y-8">
            <div className="flex gap-4">
                <img
                    alt=""
                    src={data?.image}
                    className="object-cover rounded-lg w-2/5 aspect-[2/3]"
                />
                <div className="h-full w-full justify-evenly space-y-4">
                    <div className="w-2/3 text-2xl font-semibold">
                        {data?.title}
                    </div>
                    <div className="text-gray-600">{data?.description}</div>
                    {data?.price && (
                        <div className="flex items-start font-bold gap-1">
                            <span className="text-xs">$</span>
                            <span className="leading-4 text-xl">
                                {data?.price}
                            </span>
                        </div>
                    )}
                </div>
            </div>
            <div className="flex">
                <div className="w-full space-y-4">
                    <div className="text-lg font-semibold">Mood</div>
                    <div className="flex gap-2">
                        {variants.mood.map((el: any, index: any) => {
                            return (
                                <ButtonRounded
                                    onclick={() => setMood(el)}
                                    active={el == mood}
                                    key={index}
                                >
                                    {el == "hot" ? <BsFire className="text-red-500" /> : <IoMdSnow className="text-blue-400" />}
                                </ButtonRounded>
                            );
                        })}
                    </div>
                </div>
                <div className="w-full space-y-4">
                    <div className="text-lg font-semibold">Size</div>
                    <div className="flex gap-2">
                        {variants.size.map((el: any, index: any) => {
                            return (
                                <ButtonRounded
                                    onclick={() => setSize(el)}
                                    active={el == size}
                                    key={index}
                                >
                                    <span className="uppercase font-semibold">
                                        {el}
                                    </span>
                                </ButtonRounded>
                            );
                        })}
                    </div>
                </div>
            </div>
            <div className="flex">
                <div className="w-full space-y-4">
                    <div className="text-lg font-semibold">Sugar</div>
                    <div className="flex gap-2">
                        {variants.sugar.map((el: any, index: any) => {
                            return (
                                <ButtonRounded
                                    onclick={() => setSugar(el)}
                                    active={el == sugar}
                                    key={index}
                                    className="odd:text-red-500"
                                >
                                    <span className="text-sm">{el}</span>
                                </ButtonRounded>
                            );
                        })}
                    </div>
                </div>
                <div className="w-full space-y-4">
                    <div className="text-lg font-semibold">Ice</div>
                    <div className="flex gap-2">
                        {variants.ice.map((el: any, index: any) => {
                            return (
                                <ButtonRounded
                                    onclick={() => setIce(el)}
                                    active={el == ice}
                                    key={index}
                                    className="odd:text-red-500"
                                >
                                    <span className="text-sm">{el}</span>
                                </ButtonRounded>
                            );
                        })}
                    </div>
                </div>
            </div>
            {mood && size && sugar && ice && (
                <Button text="Add to Billing" onclick={() => handleClick()} />
            )}
        </div>
    );
};

export default Card;
