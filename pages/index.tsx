import Card from "@/components/Card";
import Sidebar from "@/components/Sidebar";
import Summary from "@/components/Summary";
import items from "../data/items.json";
import { useEffect, useState } from "react";
import CategoryIcon from "@/components/CategoryIcon";
import Search from "@/components/Search";
import { Modal } from "@/components/Modal";
import { BsFillGridFill } from "react-icons/bs";
import {
    GiCoffeeCup,
    GiManualJuicer,
    GiMilkCarton,
    GiChipsBag,
    GiBowlOfRice,
    GiCakeSlice,
} from "react-icons/gi";

export default function Home() {
    const categories = [
        { name: "all", image: <BsFillGridFill className="w-full h-full" /> },
        { name: "coffee", image: <GiCoffeeCup className="w-full h-full" /> },
        { name: "juice", image: <GiManualJuicer className="w-full h-full" /> },
        {
            name: "milk bassed",
            image: <GiMilkCarton className="w-full h-full" />,
        },
        { name: "snack", image: <GiChipsBag className="w-full h-full" /> },
        { name: "rice", image: <GiBowlOfRice className="w-full h-full" /> },
        { name: "dessert", image: <GiCakeSlice className="w-full h-full" /> },
    ];

    const [item, setItem] = useState<any>(items);
    const [selected, setSelected] = useState<any>([]);
    const [category, setCategory] = useState<any>("coffee");
    const [modal, setModal] = useState<boolean>(false);

    const handleSearch = (val: any) => {
        if (val) {
            let result = items.filter((el: any) => el.title.toLowerCase().includes(val.toLowerCase()));

            setItem(result);
            setCategory("all");
        } else {
            setItem(item);
        }
    };

    const handleSelected = (val: any) => {
        selected.filter((el: any) => el.title == val.title).length > 0
            ? null
            : setSelected((prev: any) => [...prev, val]);
    };

    useEffect(() => {
        if (category == "all") {
            setItem(items);
        } else {
            setItem(items.filter((el: any) => el.category == category));
        }
    }, [category]);

    useEffect(() => {
        console.log(selected);
    }, [selected]);

    return (
        <div className="flex relative">
            <Sidebar />
            <main className="overflow-y-auto w-full relative space-y-4 py-4 px-8">
                <div className="sticky top-0 w-full space-y-8">
                    <div className="flex justify-between items-center">
                        <h1 className="w-full text-3xl font-semibold">
                            Product
                        </h1>
                        <Search onChange={(e: any) => handleSearch(e)} />
                    </div>
                    <div className="flex justify-between gap-12">
                        {categories.map((el: any, index: any) => {
                            return (
                                <CategoryIcon
                                    onClick={() => setCategory(el.name)}
                                    icon={el.image}
                                    text={el.name}
                                    key={index}
                                    active={el.name == category}
                                />
                            );
                        })}
                    </div>
                </div>
                <div className="w-full space-y-8">
                    <div className="flex justify-between">
                        <h2 className="text-xl font-semibold capitalize">
                            {category} Menu
                        </h2>
                        <div className="text-gray-600 capitalize">
                            {item.length} Coffees result
                        </div>
                    </div>
                    <div className="grid grid-cols-2 gap-12">
                        {item.map((el: any, index: any) => {
                            return (
                                <Card
                                    action={(e: any) => handleSelected(e)}
                                    key={index}
                                    data={el}
                                />
                            );
                        })}
                    </div>
                </div>
            </main>
            <Summary data={selected} setModal={(e: any) => setModal(e)} />

            <Modal
                show={modal}
                width="w-[500px]"
                onClose={() => setModal(false)}
            >
                <div className="rounded-lg bg-white p-6 space-y-6">
                    <div className="w-full text-center text-4xl font-bold italic">
                        K
                    </div>
                    <div className="text-center">
                        <div>K COFFEE SHOP</div>
                        <div>1879, HIGH HOUSE RD NEW HILL, NC, 27513</div>
                        <div>(919)-888-8382</div>
                    </div>
                    <div>
                        <div>ORDER : 64</div>
                        <div className="flex justify-between">
                            <div>10/19/2023 12:35 PM</div>
                            <div>HOST : JELLY GRANDE</div>
                        </div>
                    </div>
                    <div className="border-y py-12 space-y-4">
                        {selected.map((el: any, index: any) => {
                            return (
                                <div
                                    className="flex justify-between"
                                    key={index}
                                >
                                    <span>{el.title}</span>
                                    <span>$ {el.price}</span>
                                </div>
                            );
                        })}
                    </div>
                    <div>
                        <div className="flex justify-between">
                            <div>Subtotal</div>
                            <div>
                                <span className="text-xs">$</span>
                                {selected
                                    ?.map((el: any) => el.price)
                                    .reduce((a: any, b: any) => a + b, 0)}
                            </div>
                        </div>
                        <div className="flex justify-between text-gray-600">
                            <div>Tax</div>
                            <div>
                                <span className="text-xs">$</span>
                                {(selected
                                    ?.map((el: any) => el.price)
                                    .reduce((a: any, b: any) => a + b, 0) / 10).toFixed(2)}
                            </div>
                        </div>
                        <div className="flex justify-between font-bold text-lg">
                            <div>Total</div>
                            <div>
                                <span className="text-xs">$</span>
                                <span>
                                    {((selected
                                        ?.map((el: any) => el.price)
                                        .reduce((a: any, b: any) => a + b, 0) *
                                        110) /
                                        100).toFixed(3)}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="text-center pt-16">
                        <div>CUSTOMER COPY</div>
                        <div>THANKS FOR VISITING</div>
                        <div>K COFFEE SHOP</div>
                    </div>
                </div>
            </Modal>
        </div>
    );
}
